class UsersController < ApplicationController
	def index
		@Users = User.all
	end
	def new
		@user = User.new
	end
	def create
		@user = User.new(user_params);
		@user.save
		redirect_to(action: "index")
	end
	def user_params
		params.require(:user).permit(:name, :last_name, :email, :birthdate, :password, :password_confirmation)
	end
end
