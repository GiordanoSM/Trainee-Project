class Payment < ApplicationRecord
  belongs_to :order
  has_one :card, dependent: :destroy
  enum type_: [:card, :bill_exchange]
  enum status: [:waiting, :confirmed, :canceled, :refunded]
end
