class User < ApplicationRecord
  authenticates_with_sorcery!
	attr_accessor :skip_password #usaremos isso em casos que precisemos alterar algo de um usuário sem autenticar com a senha dele, esse attr_accessor adiciona um método que não está na tabela Users
	validates :password, length: { minimum: 8 }, if: -> { new_record? || changes[:crypted_password] }, unless: :skip_password #verifica se a senha tem pelo menos 8 caracteres, em minimum: 8 podemos alterar esse mínimo
	validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] } #não existe um password_confirmation na tabela de Users e com isso essa linha exige que seja passado uma confirmação apenas para verificar se não houve erro de digitação
	validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] } # complementa a linha anterior
 	validates :email, uniqueness: true #exige que o email seja único
 	validates :name, presence:true
	validates :last_name, presence:true
	enum role: [:normal, :admin, :super_admin]
 	has_one :adress, dependent: :destroy
	has_many :orders, dependent: :destroy
	has_many :products, dependent: :destroy
	has_one_attached :avatar, dependent: :destroy
end
