# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user1 = User.create(name: "Luisa", last_name: "Padilha", email: "lulu@gmail.com", birthdate:"16/04/1999",
	password: "Lulu1920", password_confirmation: "Lulu1920")

user2 = User.create(name: "Cecilia", last_name: "Cipriano", email: "cecilia@gmail.com", 
	birthdate:"29/10/1998", password: "Cecicoc20", password_confirmation: "Cecicoc20")

product1 = Product.create(title: "Natural Mask", type_: "flexible", category: "design_tech",
	final_date: "25/12/2018", min_value: 50, max_value: 1000, tax: 0.1, qnt_raised: 572.90, goal: 2000, user_id: user1.id)

order1 = Order.create(user_id: user2.id, product_id: product1.id)
order2 = Order.create(user_id: user2.id, product_id: product1.id)

Adress.create(postal_code: "22222222", country: "Brasil", state: "DF", city: "Brasilia", user_id: user1.id)

Adress.create(postal_code: "33333333", country: "Brasil", state: "DF", city: "Brasilia", user_id: user2.id)

payment1 = Payment.create(type_: "card", code: "192398129", value: 540, status: "confirmed", order_id: order1.id)

payment2 = Payment.create(type_: "bill_exchange", code: "9829193898", value: 780, status: "waiting", order_id: order2.id)

Card.create(name: "CECILIA O. CIPRIANO", expire_date: "12/19", number: 123141234, payment_id: payment1.id)